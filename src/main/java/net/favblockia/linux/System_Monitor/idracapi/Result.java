package net.favblockia.linux.System_Monitor.idracapi;

public class Result {
	
	String SensorName;
	String Value;
	
	public Result(String SensorName, String Value) {
		this.SensorName = SensorName;
		this.Value = Value;
	}
	
	public String getSensorName() {
		return this.SensorName;
	}
	
	public String getValue() {
		return this.Value;
	}
	
}
