package net.favblockia.linux.System_Monitor.mysql;

import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CheckTable {
	
	public static void checkTable() {
		try {
				if (Connect.getConnection() != null && !Connect.getConnection().isClosed()) {
					return;
				}
				Class.forName("com.mysql.jdbc.Driver");
				Connect.setConnection(DriverManager.getConnection("jdbc:mysql://" + Connect.host + ":" + Connect.port + "/" + Connect.database
						+ "?useSSL=false", Connect.username, Connect.password));
			if (!tableExists(Connect.table)) {
				PreparedStatement insert = Connect.getConnection()
						.prepareStatement("CREATE TABLE "+Connect.table+"(TEMP INT NOT NULL, SENSOR TEXT, TIME TIMESTAMP NOT NULL)");
				insert.executeUpdate();
				System.out.println("Table Created");
			}
		} catch (SQLException|ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}

	public static boolean tableExists(String table) {
		DatabaseMetaData meta = null;
		ResultSet res;
		try {
			String tab = "";
			meta = Connect.getConnection().getMetaData();
			res = meta.getTables(null, null, table, new String[] { "TABLE" });
			while (res.next()) {
				tab = res.getString("TABLE_NAME");
			}
			if (tab.equals(table)) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
