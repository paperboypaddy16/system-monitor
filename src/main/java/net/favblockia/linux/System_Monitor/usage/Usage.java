package net.favblockia.linux.System_Monitor.usage;

import java.lang.management.ManagementFactory;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.sun.management.OperatingSystemMXBean;

import net.favblockia.linux.System_Monitor.mysql.Connect;

import java.util.Timer;
import java.util.TimerTask;

public class Usage {

	public static void getCPUUsage() {
		System.out.println("Starting CPU Usage Monitoring!");
		Timer timer = new Timer();
		timer.schedule(new SendCPUUsage(), 0, 10000);
	}
	
}

class SendCPUUsage extends TimerTask {
	public void run() {
		OperatingSystemMXBean operatingSystemMXBean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		try {
		upload("cpuusage", "CPU Usage", String.format("%.2f", operatingSystemMXBean.getCpuLoad()*100)+"");
		upload("memoryusage", "Total Memory", operatingSystemMXBean.getTotalMemorySize()/1024/1024+"");
		upload("memoryusage", "Memory Used", ((operatingSystemMXBean.getTotalMemorySize()-operatingSystemMXBean.getFreeMemorySize())/1024/1024)+"");
		upload("swapusage", "Total Swap", operatingSystemMXBean.getTotalSwapSpaceSize()/1024/1024+"");
		upload("swapusage", "Swap Used", ((operatingSystemMXBean.getTotalSwapSpaceSize()-operatingSystemMXBean.getFreeSwapSpaceSize())/1024/1024)+"");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void upload(String Table, String SensorName, String Value) {
		try {
			if (Connect.getConnection() == null) {
				return;
			}
			if (Value == null) {
				return;
			}
			PreparedStatement insert = Connect.getConnection()
					.prepareStatement("INSERT INTO " + Table + " (VALUE,SENSOR,TIME) VALUES (?,?,?)");
			insert.setDouble(1, (Double) Double.parseDouble(Value));
			insert.setString(2, SensorName);
			insert.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			insert.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}
