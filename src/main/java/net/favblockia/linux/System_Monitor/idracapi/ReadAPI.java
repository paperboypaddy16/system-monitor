package net.favblockia.linux.System_Monitor.idracapi;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;

import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class ReadAPI {

	public static ArrayList<Result> OutPutAllValues() {
		String address = "https://192.168.8.23/redfish/v1";
		ArrayList<ValueSet> APIList = new ArrayList<ValueSet>();
		APIList.add(new ValueSet("PSU","root", "calvin", address,
				"/Chassis/System.Embedded.1/Power/PowerControl", "PowerConsumedWatts", "", 0));
		APIList.add(new ValueSet("Ambient Temp", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"ReadingCelsius", "Temperatures", 0));
		APIList.add(new ValueSet("CPU 1", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"ReadingCelsius", "Temperatures", 1));
		APIList.add(new ValueSet("CPU 2", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"ReadingCelsius", "Temperatures", 2));
		APIList.add(new ValueSet("Fan 0", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"Reading", "Fans", 0));
		APIList.add(new ValueSet("Fan 1", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"Reading", "Fans", 1));
		APIList.add(new ValueSet("Fan 2", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"Reading", "Fans", 2));
		APIList.add(new ValueSet("Fan 3", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"Reading", "Fans", 3));
		APIList.add(new ValueSet("Fan 4", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"Reading", "Fans", 4));
		APIList.add(new ValueSet("Fan 5", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"Reading", "Fans", 5));
		APIList.add(new ValueSet("Fan 6", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"Reading", "Fans", 6));
		APIList.add(new ValueSet("Fan 7", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"Reading", "Fans", 7));
		APIList.add(new ValueSet("Fan 8", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"Reading", "Fans", 8));
		APIList.add(new ValueSet("Fan 9", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"Reading", "Fans", 9));
		APIList.add(new ValueSet("Fan 10", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"Reading", "Fans", 10));
		APIList.add(new ValueSet("Fan 11", "root", "calvin", address, "/Chassis/System.Embedded.1/Thermal",
				"Reading", "Fans", 11));
		ArrayList<Result> Results = getAPIValues(APIList);
		//for (Result i : Results) {
		//	System.out.println(i.getSensorName() + ": " + i.Value);
		//}
		return Results;
	}

	public static ArrayList<Result> getAPIValues(ArrayList<ValueSet> Values) {
		ArrayList<Result> results = new ArrayList<Result>();
		HashMap<String, String> oldjson = new HashMap<String, String>();
		for (int i = 0; Values.size() > i; i++) {
			if (!oldjson.containsKey(Values.get(i).getAddress())) {
				if (!Values.get(i).getUsername().isEmpty() || !Values.get(i).getPassword().isEmpty()) {
					RestAssured.baseURI = Values.get(i).getbaseURI();
					RestAssured.useRelaxedHTTPSValidation();
					PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
					authScheme.setUserName(Values.get(i).getUsername());
					authScheme.setPassword(Values.get(i).getPassword());
					RestAssured.authentication = authScheme;
				}

				RequestSpecification httpRequest = RestAssured.given();
				Response response = httpRequest.get(Values.get(i).getAddress());

				// Retrieve the body of the Response
				@SuppressWarnings("rawtypes")
				ResponseBody body = response.getBody();
				oldjson.put(Values.get(i).getAddress(), body.asString());

				// By using the ResponseBody.asString() method, we can convert the body
				// into the string representation.
				results.add(new Result(Values.get(i).getSensorName(), decodeJSON(body.asString(), Values.get(i).getObject(), Values.get(i).getArray(),
						Values.get(i).getAIndex())));
			} else {
				results.add(new Result(Values.get(i).getSensorName(),decodeJSON(oldjson.get(Values.get(i).getAddress()), Values.get(i).getObject(), Values.get(i).getArray(),
						Values.get(i).getAIndex())));
			}
		}
		return results;
	}

	public static String decodeJSON(String JSON, String Object, String Array, int Index) {
		if (Array.isEmpty()) {
			JSONObject jsonObject = new JSONObject(JSON);

			return jsonObject.get(Object).toString();
		} else {
			JSONObject jsonObject = new JSONObject(JSON);

			JSONArray jsonArray = (JSONArray) jsonObject.get(Array);
			JSONObject jsonObject2 = new JSONObject(jsonArray.get(Index).toString());
			return jsonObject2.get(Object).toString();
		}
	}

	public static void main(String[] args) {
		OutPutAllValues();
	}

}
