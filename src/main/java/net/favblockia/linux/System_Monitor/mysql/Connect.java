package net.favblockia.linux.System_Monitor.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Connect {
	private static Connection connection;
	public static String host = "192.168.8.37";
	public static String database = "server_monitor";
	public static String username = "ServerMonitor";
	public static String password = "8SZJYg0HIy1H6xQT";
	public static String table = "cputemps";
	public static int port = 3306;
	
	public static void mysqlSetup() {
		try {
				if (getConnection() != null && !getConnection().isClosed()) {
					return;
				}
				Class.forName("com.mysql.jdbc.Driver");
				setConnection(DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + "?useSSL=false", username, password));
				System.out.println("MySQL Connected");
				if (!databaseExists(database)) {
				PreparedStatement insert = getConnection().prepareStatement("CREATE DATABASE "+database);
				insert.executeUpdate();
				System.out.println("MySQL Database Created");
				}
			getConnection().close();
			CheckTable.checkTable();
		} catch (SQLException|ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}
	
	public static boolean databaseExists(String database) {
		try {
			PreparedStatement statement = getConnection().prepareStatement("SHOW DATABASES");
            ResultSet results = statement.executeQuery();
            while (results.next()) {
            	if (results.getString("Database").equalsIgnoreCase(database)) {
            		return true;
            	}
            }
            return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static Connection getConnection() {
		return connection;
	}

	public static void setConnection(Connection conn) {
		connection = conn;
	}
	
}
