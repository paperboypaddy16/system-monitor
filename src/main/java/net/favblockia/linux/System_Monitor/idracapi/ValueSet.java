package net.favblockia.linux.System_Monitor.idracapi;

public class ValueSet {
	
	String SensorName;
	String Username;
	String Password;
	String baseURI;
	String getAddress;
	String Object;
	String Array;
	int AIndex;
	
	public ValueSet(String SensorName, String Username, String Password, String baseURI, String getAddress,
			String Object, String Array, int AIndex) {
		this.SensorName = SensorName;
		this.Username = Username;
		this.Password = Password;
		this.baseURI = baseURI;
		this.getAddress = getAddress;
		this.Object = Object;
		this.Array = Array;
		this.AIndex = AIndex;
	}
	
	public String getSensorName() {
		return this.SensorName;
	}
	
	public String getUsername() {
		return this.Username;
	}
	
	public String getPassword() {
		return this.Password;
	}
	
	public String getbaseURI() {
		return this.baseURI;
	}
	
	public String getAddress() {
		return this.getAddress;
	}
	
	public String getObject() {
		return this.Object;
	}
	
	public String getArray() {
		return this.Array;
	}
	
	public int getAIndex() {
		return this.AIndex;
	}
}
