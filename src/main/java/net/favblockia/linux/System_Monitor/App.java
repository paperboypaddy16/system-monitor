package net.favblockia.linux.System_Monitor;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.profesorfalken.jsensors.JSensors;
import com.profesorfalken.jsensors.model.components.Components;
import com.profesorfalken.jsensors.model.components.Cpu;
import com.profesorfalken.jsensors.model.sensors.Temperature;

import net.favblockia.linux.System_Monitor.idracapi.ReadAPI;
import net.favblockia.linux.System_Monitor.idracapi.Result;
import net.favblockia.linux.System_Monitor.mysql.Connect;
import net.favblockia.linux.System_Monitor.usage.Usage;

public class App {
	public static void main(String[] args) {
		Connect.mysqlSetup();
		if (args.length > 0) {
			for (final String arg : args) {
				if ("--cpuonly".equals(arg)) {
					monitorCPU();
				}
				if ("--apionly".equals(arg)) {
					monitorAPI();
				}
				if ("--usage".equals(arg)) {
					monitorUsage();
				}
			}
		} else {
			runAll();
		}
	}

	public static void runAll() {
		monitorCPU();
		monitorAPI();
		monitorUsage();
	}

	public static void monitorCPU() {
		System.out.println("Starting System Monitoring!");
		Timer timer = new Timer();
		timer.schedule(new UploadResults(), 0, 10000);
	}

	public static void monitorAPI() {
		System.out.println("Starting API Monitoring!");
		Timer timer = new Timer();
		timer.schedule(new UploadAPIResults(), 0, 30000);
	}

	public static void monitorUsage() {
		Usage.getCPUUsage();
	}
}

class UploadResults extends TimerTask {
	public void run() {
		Components components = JSensors.get.components();
		List<Cpu> cpus = components.cpus;
		if (cpus != null) {
			for (final Cpu cpu : cpus) {
				if (cpu.sensors != null) {

					// Print temperatures
					List<Temperature> temps = cpu.sensors.temperatures;
					for (final Temperature temp : temps) {
						try {
							if (Connect.getConnection() == null) {
								return;
							}
							PreparedStatement insert = Connect.getConnection().prepareStatement(
									"INSERT INTO " + Connect.table + " (TEMP,SENSOR,TIME) VALUES (?,?,?)");
							insert.setInt(1, (int) Math.round(temp.value));
							insert.setString(2, temp.name);
							insert.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
							insert.executeUpdate();
						} catch (SQLException ex) {
							ex.printStackTrace();
						}
					}
				}
			}
		}
	}
}

class UploadAPIResults extends TimerTask {
	public void run() {
		try {
			ArrayList<Result> Results = ReadAPI.OutPutAllValues();
			for (Result i : Results) {
				switch (i.getSensorName().charAt(0)) {
				case 'P':
					upload("apipsu", i.getSensorName(), i.getValue());
					break;
				case 'A':
					upload("apiamb", i.getSensorName(), i.getValue());
					break;
				case 'C':
					upload("apicpu", i.getSensorName(), i.getValue());
					break;
				case 'F':
					upload("apifan", i.getSensorName(), i.getValue());
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void upload(String Table, String SensorName, String Value) {
		try {
			if (Connect.getConnection() == null) {
				return;
			}
			if (Value == null) {
				return;
			}
			PreparedStatement insert = Connect.getConnection()
					.prepareStatement("INSERT INTO " + Table + " (TEMP,SENSOR,TIME) VALUES (?,?,?)");
			insert.setInt(1, (int) Integer.parseInt(Value));
			insert.setString(2, SensorName);
			insert.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			insert.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}
